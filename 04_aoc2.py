def adj(s):
    last = None
    repeat_len = 0
    for c in s:
        if c == last:
            repeat_len += 1
        else:
            if repeat_len == 1:
                return True
            repeat_len = 0
        last = c
    return repeat_len == 1

def decr(s):
    last = chr(0)
    for c in s:
        if c < last:
            return True
        last = c
    return False

n = sum(adj(str(i)) and not decr(str(i)) for i in range(278384, 824795 + 1))
print(n)
