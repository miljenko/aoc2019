with open('02_input.txt') as f:
    l = [int(n) for n in f.read().split(',')]

l[1] = 12
l[2] = 2
pos = 0

while True:
    op = l[pos]
    if op == 99:
        break
    if op == 1:
        l[l[pos+3]] = l[l[pos+1]] + l[l[pos+2]]
    elif op == 2:
        l[l[pos+3]] = l[l[pos+1]] * l[l[pos+2]]
    pos += 4

print(l[0])
