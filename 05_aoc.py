with open('05_input.txt') as f:
    l = [int(n) for n in f.read().split(',')]

INSTR_SIZE = {
    1: 4,
    2: 4,
    3: 2,
    4: 2
}

pos = 0
while True:
    op = l[pos]
    if op == 99:
        break

    param_modes = '000'
    if op > 99:
        param_modes = str(op)[:-2][::-1].ljust(3, '0')
        op %= 100

    size = INSTR_SIZE[op]

    p1 = l[l[pos+1]] if param_modes[0] == '0' else l[pos+1]
    if size > 2:
        p2 = l[l[pos+2]] if param_modes[1] == '0' else l[pos+2]

    if op == 1:
        l[l[pos+3]] = p1 + p2
    elif op == 2:
        l[l[pos+3]] = p1 * p2
    elif op == 3:
        l[l[pos+1]] = 1
    elif op == 4:
        print(l[l[pos+1]])

    pos += size
