from collections import defaultdict

orbits = defaultdict(list)
with open('06_input.txt') as f:
    for line in f:
        o1, o2 = line.strip().split(')')
        orbits[o1].append(o2)

def dfs(obj, path, num):
    nxt_path = path + [obj]
    s = 0
    for nxt in orbits[obj]:
        s += dfs(nxt, nxt_path, len(nxt_path))
    return num + s

num_orbits = dfs('COM', [], 0)
print(num_orbits)
