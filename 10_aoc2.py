grid = {}
asteroid_locations = set()

def can_see(x0, y0, x1, y1):
    for tx in range(min(x0, x1), max(x0, x1)+1):
        for ty in range(min(y0, y1), max(y0, y1)+1):
            if (tx, ty) == (x0, y0) or (tx, ty) == (x1, y1):
                continue
            if grid[tx, ty] == '#' and (x1-x0)*(ty-y0)-(y1-y0)*(tx-x0) == 0:
                return False

    return True

with open('10_input.txt') as f:
    for y, line in enumerate(f):
        for x, c in enumerate(line.strip()):
            grid[x, y] = c
            if c == '#':
                asteroid_locations.add((x, y))

LASER_X, LASER_Y = 19, 11
visible = set()
for x1, y1 in asteroid_locations:
    if (LASER_X, LASER_Y) == (x1, y1):
        continue
    if can_see(LASER_X, LASER_Y, x1, y1):
        visible.add((x1, y1))

slopes = []
for vx, vy in visible:
    if vx < LASER_X and vy < LASER_Y:
        vxr = LASER_X - vx
        vyr = vy - LASER_Y
        slopes.append((vx, vy, vyr/vxr))

s = sorted(slopes, key=lambda t: -t[2])
for sl in s:
    print(sl)
v200x, v200y, v200s = s[-(len(visible)-200) - 1]
print(v200x*100 + v200y)

# for row in range(23):
#     for col in range(23):
#         print('O' if (col, row) in visible else grid[col, row], end='')
#     print()
