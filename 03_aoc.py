def dist(posx, posy):
    return abs(posx) + abs(posy)

with open('03_input.txt') as f:
    wires = [line.split(',') for line in f]

grid = {(0, 0): 'o'}
dirs = {
    'U': (0, 1),
    'R': (1, 0),
    'D': (0, -1),
    'L': (-1, 0)
}

for n, w in enumerate(wires, start=1):
    x, y = 0, 0
    for p in w:
        d = dirs[p[0]]
        for i in range(int(p[1:])):
            x += d[0]
            y += d[1]
            current = grid.get((x, y))
            if not current:
                grid[x, y] = n
            elif current != n:
                grid[x, y] = 'X'

closest = min(dist(x, y) for (x, y), val in grid.items() if val == 'X')
print(closest)
