from collections import Counter

W, H = 25, 6
layers = []
counters = []
with open('08_input.txt') as f:
    while True:
        l = f.read(W*H).strip()
        if not l:
            break
        layers.append(l)
        counters.append(Counter(l))

min0_cnt, min0_idx = min((c['0'], i) for i, c in enumerate(counters))
print(counters[min0_idx]['1'] * counters[min0_idx]['2'])
