import re
from functools import reduce
from itertools import combinations
from math import gcd

def lcm(a, b):
    return (a*b) // gcd(a, b)

moons = []
velocities = [[0 for i in range(3)] for j in range(4)]
with open('12_input.txt') as f:
    for line in f:
        moons.append([int(n) for n in re.findall(r'-?\d+', line)])

inital = [m[:] for m in moons]
axis_period = [-1] * 3
t = 0
while True:
    for (m1i, m1), (m2i, m2) in combinations(enumerate(moons), 2):
        for vi, (c1, c2) in enumerate(zip(m1, m2)):
            if c1 < c2:
                velocities[m1i][vi] += 1
                velocities[m2i][vi] -= 1
            elif c1 > c2:
                velocities[m1i][vi] -= 1
                velocities[m2i][vi] += 1
    for moon, v in zip(moons, velocities):
        for i in range(3):
            moon[i] += v[i]

    t += 1

    for d in range(3):
        if axis_period[d] == -1 and all(m[d] == inital[i][d] and velocities[i][d] == 0 for i, m in enumerate(moons)):
            axis_period[d] = t

    if all(p > -1 for p in axis_period):
        break

# print(t)
# print(axis_period)
print(reduce(lcm, axis_period))
