W, H = 25, 6
layers = []
with open('08_input.txt') as f:
    while True:
        l = f.read(W*H).strip()
        if not l:
            break
        layers.append(l)

decoded = [next(p for p in z if p != '2') for z in zip(*layers)]
for r in range(H):
    print(''.join(' ' if p == '0' else '#' for p in decoded[r*W:(r+1)*W]))
