from itertools import permutations

with open('07_input.txt') as f:
    prog = [int(n) for n in f.read().split(',')]

INSTR_SIZE = {
    1: 4,
    2: 4,
    3: 2,
    4: 2,
    5: 3,
    6: 3,
    7: 4,
    8: 4
}

def amp(phase, inp):
    l = prog[:]
    pos = 0
    first = True
    while True:
        op = l[pos]
        if op == 99:
            break

        param_modes = '000'
        if op > 99:
            param_modes = str(op)[:-2][::-1].ljust(3, '0')
            op %= 100

        size = INSTR_SIZE[op]

        p1 = l[l[pos+1]] if param_modes[0] == '0' else l[pos+1]
        if size > 2:
            p2 = l[l[pos+2]] if param_modes[1] == '0' else l[pos+2]

        if op == 1:
            l[l[pos+3]] = p1 + p2
        elif op == 2:
            l[l[pos+3]] = p1 * p2
        elif op == 3:
            l[l[pos+1]] = phase if first else inp
            first = False
        elif op == 4:
            return l[l[pos+1]]
        elif op == 5:
            if p1 != 0:
                pos = p2
                continue
        elif op == 6:
            if p1 == 0:
                pos = p2
                continue
        elif op == 7:
            l[l[pos+3]] = 1 if p1 < p2 else 0
        elif op == 8:
            l[l[pos+3]] = 1 if p1 == p2 else 0

        pos += size

max_signal = 0
for perm in permutations(range(5)):
    i = 0
    for p in perm:
        i = amp(p, i)
    if i > max_signal:
        max_signal = i

print(max_signal)
