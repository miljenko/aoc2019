from collections import defaultdict

orbits = defaultdict(list)
with open('06_input.txt') as f:
    for line in f:
        o1, o2 = line.strip().split(')')
        orbits[o1].append(o2)

def dfs(obj, target, path):
    nxt_path = path + [obj]
    if obj == target:
        return nxt_path
    for nxt in orbits[obj]:
        val = dfs(nxt, target, nxt_path)
        if val:
            return val
    return None

path_you = dfs('COM', 'YOU', [])
path_san = dfs('COM', 'SAN', [])
for i, (c1, c2) in enumerate(zip(path_you, path_san)):
    if c1 != c2:
        print(len(path_you) - i + len(path_san) - i - 2)
        break
