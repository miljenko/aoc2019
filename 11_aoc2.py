from collections import defaultdict

l = defaultdict(int)
with open('11_input.txt') as f:
    for i, n in enumerate(f.read().split(',')):
        l[i] = int(n)

grid = defaultdict(int)
grid[0, 0] = 1
painted = set()

moves = { # direction: (dx, dy)
    '<': (-1, 0),
    '>': (1, 0),
    '^': (0, -1),
    'v': (0, 1)
}

turns = { # (direction, turn): new_direction
    ('<', 0): 'v',
    ('>', 0): '^',
    ('^', 0): '<',
    ('v', 0): '>',
    ('<', 1): '^',
    ('>', 1): 'v',
    ('^', 1): '>',
    ('v', 1): '<'
}

INSTR_SIZE = {
    1: 4,
    2: 4,
    3: 2,
    4: 2,
    5: 3,
    6: 3,
    7: 4,
    8: 4,
    9: 2
}
WRITABLE = {1, 2, 3, 7, 8}

def param_val(loc, mode, prog, rb, write=False):
    pval = prog[loc]
    if mode == '0':
        return pval if write else prog[pval]
    if mode == '1':
        return pval
    if mode == '2':
        return rb + pval if write else prog[rb + pval]

class Robot():
    def __init__(self):
        self.relative_base = 0
        self.pos = 0

    def move(self, inp):
        while True:
            op = l[self.pos]
            if op == 99:
                return -1

            param_modes = '000'
            if op > 99:
                param_modes = str(op)[:-2][::-1].ljust(3, '0')
                op %= 100

            size = INSTR_SIZE[op]

            p1 = param_val(self.pos+1, param_modes[0], l, self.relative_base)
            if size > 2:
                p2 = param_val(self.pos+2, param_modes[1], l, self.relative_base)
            if op in WRITABLE:
                wpos = param_val(self.pos+size-1, param_modes[size-2], l, self.relative_base, write=True)

            if op == 1:
                l[wpos] = p1 + p2
            elif op == 2:
                l[wpos] = p1 * p2
            elif op == 3:
                l[wpos] = inp
            elif op == 4:
                self.pos += size
                return p1
            elif op == 5:
                if p1 != 0:
                    self.pos = p2
                    continue
            elif op == 6:
                if p1 == 0:
                    self.pos = p2
                    continue
            elif op == 7:
                l[wpos] = 1 if p1 < p2 else 0
            elif op == 8:
                l[wpos] = 1 if p1 == p2 else 0
            elif op == 9:
                self.relative_base += p1

            self.pos += size

r = Robot()
x, y, d = 0, 0, '^'
minx, miny, maxx, maxy = 0, 0, 0, 0
while True:
    color = r.move(grid[x, y])
    if color == -1:
        break
    turn = r.move(grid[x, y])
    grid[x, y] = color
    painted.add((x, y))
    d = turns[d, turn]
    dx, dy = moves[d]
    x += dx
    y += dy

    if x < minx: minx = x
    if x > maxx: maxx = x
    if y < miny: miny = y
    if y > maxy: maxy = y

for y in range(miny, maxy+1):
    for x in range(minx, maxx+1):
        print('#' if grid[x, y] else '.', end='')
    print()
