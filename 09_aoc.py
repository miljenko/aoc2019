from collections import defaultdict

l = defaultdict(int)
with open('09_input.txt') as f:
    for i, n in enumerate(f.read().split(',')):
        l[i] = int(n)

INSTR_SIZE = {
    1: 4,
    2: 4,
    3: 2,
    4: 2,
    5: 3,
    6: 3,
    7: 4,
    8: 4,
    9: 2
}
WRITABLE = {1, 2, 3, 7, 8}

def param_val(loc, mode, prog, rb, write=False):
    pval = prog[loc]
    if mode == '0':
        return pval if write else prog[pval]
    if mode == '1':
        return pval
    if mode == '2':
        return rb + pval if write else prog[rb + pval]

relative_base = 0
pos = 0
while True:
    op = l[pos]
    if op == 99:
        break

    param_modes = '000'
    if op > 99:
        param_modes = str(op)[:-2][::-1].ljust(3, '0')
        op %= 100

    size = INSTR_SIZE[op]

    p1 = param_val(pos+1, param_modes[0], l, relative_base)
    if size > 2:
        p2 = param_val(pos+2, param_modes[1], l, relative_base)
    if op in WRITABLE:
        wpos = param_val(pos+size-1, param_modes[size-2], l, relative_base, write=True)

    if op == 1:
        l[wpos] = p1 + p2
    elif op == 2:
        l[wpos] = p1 * p2
    elif op == 3:
        l[wpos] = 1
    elif op == 4:
        print(p1)
    elif op == 5:
        if p1 != 0:
            pos = p2
            continue
    elif op == 6:
        if p1 == 0:
            pos = p2
            continue
    elif op == 7:
        l[wpos] = 1 if p1 < p2 else 0
    elif op == 8:
        l[wpos] = 1 if p1 == p2 else 0
    elif op == 9:
        relative_base += p1

    pos += size
