import re
from collections import defaultdict
from math import ceil

def parse(chem_str):
    ch_qty, ch = chem_str.split()
    return int(ch_qty), ch

reactions = {}
with open('14_input.txt') as f:
    for line in f:
        l = [parse(s) for s in re.findall(r'\d+ \w+', line)]
        reactions[l[-1][1]] = l[-1][0], l[:-1]

leftovers = defaultdict(int)

def step(needs):
    next_needs = defaultdict(int)
    for chem, qty in needs.items():
        # print(f'We need {qty} {chem}, already have {leftovers[chem]} {chem}')
        if chem == 'ORE':
            next_needs[chem] += qty
            continue

        qty -= leftovers[chem]
        if qty < 0:
            leftovers[chem] = -qty
            qty = 0
        else:
            leftovers[chem] = 0

        if qty == 0:
            continue

        one_reaction_qty = reactions[chem][0]
        num_reactions = ceil(qty / one_reaction_qty)

        # print(f'  => {num_reactions} reactions of {reactions[chem]}')
        if num_reactions*one_reaction_qty > qty:
            leftovers[chem] += num_reactions*one_reaction_qty - qty

        for next_qty, next_chem in reactions[chem][1]:
            next_needs[next_chem] += num_reactions*next_qty

    if len(next_needs) == 1 and 'ORE' in next_needs:
        return next_needs, True

    return next_needs, False

def calc_ore(fuel_amount):
    needed = {'FUEL': fuel_amount}
    while True:
        # print(dict(needed), dict(leftovers))
        needed, done = step(needed)
        # print('-'*80)
        if done:
            return needed['ORE']

# print(dict(needed), dict(leftovers))
ore_per_fuel = calc_ore(1)
ore_count = 1000000000000
low_limit = ore_count // ore_per_fuel

def bisect(lo, hi):
    while True:
        mid = (lo+hi) // 2
        ore_at_mid = calc_ore(mid)
        if ore_at_mid < ore_count:
            lo = mid
            # print(f'{mid} FUEL with {ore_at_mid} ORE')
        else:
            hi = mid

        if hi - lo <= 1:
            return mid

print(bisect(low_limit, low_limit*2))
