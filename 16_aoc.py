from functools import lru_cache

with open('16_input.txt') as f:
    l = [int(n) for n in f.read().strip()]

@lru_cache(maxsize=None)
def get_pattern(pos, input_len):
    base_pattern = [0, 1, 0, -1]

    pattern = [b for b in base_pattern for _ in range(pos)]
    if len(pattern) <= input_len:
        m = input_len // len(pattern) + 2
        pattern = pattern * m

    return pattern[1:input_len+1]

def fft(inp):
    ilen = len(inp)
    out = []
    for i in range(1, ilen + 1):
        pat = get_pattern(i, ilen)
        d = abs(sum(v*p for v, p in zip(inp, pat))) % 10
        # print(' + '.join(f'{v}*{p}' for v, p in zip(inp, pat)) + f' = {d}')
        out.append(d)
    return out

for _ in range(100):
    l = fft(l)

print(''.join(str(n) for n in l[:8]))
