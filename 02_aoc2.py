import sys

with open('02_input.txt') as f:
    prog = [int(n) for n in f.read().split(',')]

for noun in range(100):
    for verb in range(100):
        pos = 0
        l = prog[:]
        l[1] = noun
        l[2] = verb
        while True:
            op = l[pos]
            if op == 99:
                break
            if op == 1:
                l[l[pos+3]] = l[l[pos+1]] + l[l[pos+2]]
            elif op == 2:
                l[l[pos+3]] = l[l[pos+1]] * l[l[pos+2]]
            pos += 4
        if l[0] == 19690720:
            print(100*noun + verb)
            sys.exit()
