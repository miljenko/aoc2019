import re
import string

# (^|[^0])00([^0]|$) | (^|[^1])11([^1]|$) | ...
ADJ_RE = re.compile('|'.join(f'(^|[^{i}]){i*2}([^{i}]|$)' for i in string.digits))

def adj(s):
    return bool(ADJ_RE.search(s))

def decr(s):
    last = chr(0)
    for c in s:
        if c < last:
            return True
        last = c
    return False

n = sum(adj(str(i)) and not decr(str(i)) for i in range(278384, 824795 + 1))
print(n)
