from itertools import cycle, permutations

with open('07_input.txt') as f:
    prog = [int(n) for n in f.read().split(',')]

INSTR_SIZE = {
    1: 4,
    2: 4,
    3: 2,
    4: 2,
    5: 3,
    6: 3,
    7: 4,
    8: 4
}

def amp(n, phase, inp):
    l = amp_mem[n]
    pos = amp_pos[n]
    while True:
        op = l[pos]
        if op == 99:
            raise StopIteration

        param_modes = '000'
        if op > 99:
            param_modes = str(op)[:-2][::-1].ljust(3, '0')
            op %= 100

        size = INSTR_SIZE[op]

        p1 = l[l[pos+1]] if param_modes[0] == '0' else l[pos+1]
        if size > 2:
            p2 = l[l[pos+2]] if param_modes[1] == '0' else l[pos+2]

        if op == 1:
            l[l[pos+3]] = p1 + p2
        elif op == 2:
            l[l[pos+3]] = p1 * p2
        elif op == 3:
            l[l[pos+1]] = phase if amp_first[n] else inp
            amp_first[n] = False
        elif op == 4:
            amp_pos[n] = pos + size
            return l[l[pos+1]]
        elif op == 5:
            if p1 != 0:
                pos = p2
                continue
        elif op == 6:
            if p1 == 0:
                pos = p2
                continue
        elif op == 7:
            l[l[pos+3]] = 1 if p1 < p2 else 0
        elif op == 8:
            l[l[pos+3]] = 1 if p1 == p2 else 0

        pos += size

max_signal = 0
for perm in permutations(range(5, 10)):
    amp_mem = [prog[:] for i in range(5)]
    amp_pos = [0 for i in range(5)]
    amp_first = [True for i in range(5)]
    i = 0
    signal = 0
    try:
        for amp_n, p in cycle(zip(range(5), perm)):
            i = amp(amp_n, p, i)
            if amp_n == 4:
                signal = i
    except StopIteration:
        if signal > max_signal:
            max_signal = signal

print(max_signal)
