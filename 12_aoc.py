import re
from itertools import combinations

def sum_abs(l):
    return sum(abs(n) for n in l)

moons = []
velocities = [[0 for i in range(3)] for j in range(4)]
with open('12_input.txt') as f:
    for line in f:
        moons.append([int(n) for n in re.findall(r'-?\d+', line)])

t = 0
while t < 1000:
    for (m1i, m1), (m2i, m2) in combinations(enumerate(moons), 2):
        for vi, (c1, c2) in enumerate(zip(m1, m2)):
            if c1 < c2:
                velocities[m1i][vi] += 1
                velocities[m2i][vi] -= 1
            elif c1 > c2:
                velocities[m1i][vi] -= 1
                velocities[m2i][vi] += 1
    for moon, v in zip(moons, velocities):
        for i in range(3):
            moon[i] += v[i]

    t += 1

print(sum(sum_abs(m)*sum_abs(v) for m, v in zip(moons, velocities)))
