grid = {}
asteroid_locations = set()

def can_see(x0, y0, x1, y1):
    for tx in range(min(x0, x1), max(x0, x1)+1):
        for ty in range(min(y0, y1), max(y0, y1)+1):
            if (tx, ty) == (x0, y0) or (tx, ty) == (x1, y1):
                continue
            if grid[tx, ty] == '#' and (x1-x0)*(ty-y0)-(y1-y0)*(tx-x0) == 0:
                return False

    return True

with open('10_input.txt') as f:
    for y, line in enumerate(f):
        for x, c in enumerate(line.strip()):
            grid[x, y] = c
            if c == '#':
                asteroid_locations.add((x, y))

max_detect = 0
for x0, y0 in asteroid_locations:
    n = 0
    for x1, y1 in asteroid_locations:
        if (x0, y0) == (x1, y1):
            continue
        if can_see(x0, y0, x1, y1):
            n += 1
    if n > max_detect:
        max_detect = n
        max_loc = x0, y0

print(max_loc)
print(max_detect)
