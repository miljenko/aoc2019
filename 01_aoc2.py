def fuel_req(m):
    return int(m/3) - 2

fuel = 0
with open('01_input.txt') as f:
    for line in f:
        need = fuel_req(int(line))
        while need > 0:
            fuel += need
            need = fuel_req(need)

print(fuel)
