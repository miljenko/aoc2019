def adj(s):
    last = None
    for c in s:
        if c == last:
            return True
        last = c
    return False

def decr(s):
    last = chr(0)
    for c in s:
        if c < last:
            return True
        last = c
    return False

n = sum(adj(str(i)) and not decr(str(i)) for i in range(278384, 824795 + 1))
print(n)
